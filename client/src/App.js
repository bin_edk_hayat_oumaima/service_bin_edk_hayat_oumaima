import React, { useState } from 'react'
import './App.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Home from './components/home/Home'
import Depannage from './components/services/depannage/Depannage'
import Securite from './components/services/securite/Securite'
import Nettoyage from './components/services/nettoyage/Nettoyage'
import Petits_travaux from './components/services/petits_travaux/Petits_travaux'
import Register from './pages/register/Register'
import Login from './pages/login/Login'
import Environ from './pages/environ/Environ'
import Services from './pages/services/Services'
import NotFound from './pages/notFound/NotFound'
import ForgotPass from './pages/password/ForgotPass'
import Inscription from './pages/inscription/Inscription'
import Travaillez_chez_Bin_edk from './pages/Travaillez_chez_Bin_edk/Travaillez_chez_Bin_edk'
import Rejoignez_le_réseau_de_Pros from './pages/Rejoignez_le_réseau_de_Pros/Rejoignez_le_réseau_de_Pros'
import Charte_Qualité from './pages/Charte_Qualité/Charte_Qualité'
import CGUs from './pages/CGUs/CGUs'
import { useSelector } from 'react-redux'
import { selectUser } from './redux/UserSlice'
import { Logout } from '@mui/icons-material'
import Climatisation from './components/services/depannage/climatisation/Climatisation'
import ClimatisationLink from './components/services/depannage/climatisation/climatisationLink/ClimatisationLink'
import ClimatisationIncreS1 from './components/services/depannage/climatisation/climatisationLink/climatisationIncre/climatisationIncreS1'
import Electricité from './components/services/depannage/Electricité/Electricité'
import ClimatisationIncre2 from './components/services/depannage/climatisation/climatisationLink/climatisationIncre2/climatisationIncre2'
import ElectricitéIncre from './components/services/depannage/Electricité/ElectricitéLikn/ElectricitéIncre/ElectricitéIncre'
import ElectricitéIncre2 from './components/services/depannage/Electricité/ElectricitéLikn/ElectricitéIncre2/ElectricitéIncre2'
function App() {
  const user = useSelector(selectUser);
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' exact element={<Inscription />} />
          <Route path='/home' element={<Home />} />
          <Route path='/register' element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path='/environ' element={<Environ />} />
          <Route path='/depannage' element={<Depannage />} />
          <Route path='/securite' element={<Securite />} />
          <Route path='/nettoyage' element={<Nettoyage />} />
          <Route path='/servicesPage' element={<Services />} />
          <Route path='/petits_travaux' element={<Petits_travaux />} />
          <Route path="/forgotPass" element={<ForgotPass />} />
          <Route path="/cgus" element={<CGUs />} />
          <Route path="/charte_qualité" element={<Charte_Qualité />} />
          <Route path='/Rejoignez_le_réseau_de_Pros' element={<Rejoignez_le_réseau_de_Pros />} />
          <Route path='/Travaillez_chez_Bin_edk' element={<Travaillez_chez_Bin_edk />} />
          <Route path='/climatisation' element={<Climatisation />} />
          <Route path='/climatisationLink' element={<ClimatisationLink />} />
          <Route path='/climatisationIncre' element={<ClimatisationIncreS1 />} />
          <Route path='/climatisationIncre2' element={<ClimatisationIncre2 />} />
          <Route path='/Electricité' element={<Electricité />} />
          <Route path='/electricitéIncre' element={<ElectricitéIncre />} />
          <Route path='/electricitéIncre2' element={<ElectricitéIncre2/>}/>
{/* 
          <Route path='/InterrupteursouprisesInternetLink' element={< InterrupteursouprisesInternetLink/>} />
          <Route path='/InterrupteursouprisesInterneIncre' element={<InterrupteursouprisesInterneIncre />} />
          <Route path='/InterrupteursouprisesInterneIncre2' element={<InterrupteursouprisesInterneIncre2/>}/> */}
          <Route path='*' element={<NotFound />} />
        </Routes>
      </BrowserRouter>

    </>
  )
}
export default App