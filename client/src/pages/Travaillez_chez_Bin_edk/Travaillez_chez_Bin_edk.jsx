import React, { useState } from 'react';
import './travaillez_chez_Bin_edk.css';
import { useNavigate } from 'react-router-dom';
import Header from '../../components/common/heading/Header';
import Footer from '../../components/footer/Footer';

function Travaillez_chez_Bin_edk() {
  const [nom, setNom] = useState("");
  const [demande, setDemande] = useState("Sélectionnez ..");
  const [ville, setVille] = useState("Sélectionnez ..");
  const [tele, setTele] = useState("");
  const [email, setEmail] = useState("");
  const [theme, setTheme] = useState("");
  const navigate = useNavigate();

  const IsValidate = () => {
    let isProceed = true;
    let errorMessage = 'Veuillez remplir les champs suivants :';
    if (!nom.trim()) {
      isProceed = false;
      errorMessage += " Nom & Prénom,";
    }
    if (demande === "Sélectionnez ..") {
      isProceed = false;
      errorMessage += " demande,";
    }
    if (ville === "Sélectionnez ..") {
      isProceed = false;
      errorMessage += " Ville,";
    }
    if (!tele.trim()) {
      isProceed = false;
      errorMessage += " Téléphone,";
    }
    if (!email.trim()) {
      isProceed = false;
      errorMessage += " Email,";
    }
    if (!theme.trim()) {
      isProceed = false;
      errorMessage += " Message,";
    }
    if (!isProceed) {
      alert(errorMessage.slice(0, -1));
    } else {
      if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
        // Email est valide
      } else {
        isProceed = false;
        alert('Veuillez entrer une adresse e-mail valide.');
      }
    }
    return isProceed;
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    const Travaillez = {
      nom,
      demande,
      ville,
      tele,
      email,
      theme
    };
    if (IsValidate()) {
      fetch("http://localhost:3000/Travaillez Chez Bin_edk", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(Travaillez)
      })
        .then((res) => {
          alert('Envoyé avec succès.');
          navigate('/');
        })
        .catch((err) => {
          alert('Échec : ' + err.message);
        });
    }
  };

  return (
    <div className='travaillezTopPage'>
      <Header/>
    <div className='travaillezTop'>
      <div className='travaillezText'>
        <h1>Contactez-nous</h1>
        <p className="paragraph-27">Vous êtes <strong>client chez nous ou intéressé par nos services ?</strong> <br />Un <strong>artisan indépendant</strong> qui souhaite rejoindre nos équipes ou <strong>un candidat à la recherche d’un Job </strong>?<br />Laissez-nous un message, nous vous contacterons !</p>
      </div>
      <form onSubmit={handleSubmit} className='travaillezButtom'>
        <label>Nom & Prénom</label><br />
        <input type="text" value={nom} onChange={(e) => setNom(e.target.value)} required /><br />
        <label >Objet de la demande</label><br />
        <select value={demande} onChange={(e) => setDemande(e.target.value)}>
          <option value="Sélectionnez ..">Sélectionnez ..</option>
          <option value="Je suis un artisan et je veux rejoindre vos équipes">Je suis un artisan et je veux rejoindre vos équipes</option>
          <option value="Je suis étudiant, je recherche un stage">Je suis étudiant, je recherche un stage</option>
          <option value="Je suis à la recherche d’un Job chez Saweblia">Je suis à la recherche d’un Job chez Bin_edk</option>
          <option value="Je suis fournisseur, j’aimerai devenir votre partenaire">Je suis fournisseur, j’aimerai devenir votre partenaire</option>
          <option value="Je suis une société, je veux avoir un devis">Je suis une société, je veux avoir un devis</option>
        </select><br />
        <label>Ville</label><br />
        <select value={ville} onChange={(e) => setVille(e.target.value)}>
          <option value="Sélectionnez ..">Sélectionnez ..</option>
          <option value="Laâyoune"> Laâyoune</option>
          <option value="Boujdour">Boujdour</option>
          <option value="Es-Semara">Es-Semara</option>
          <option value="Dakhla">Dakhla</option>
          <option value="Goulmima">Goulmima</option>
          <option value="Tan Tan">Tan Tan</option>
          <option value="autre">Autre ville</option>
        </select><br />
        <label>Téléphone</label><br />
        <input type="tel" value={tele} onChange={(e) => setTele(e.target.value)} required /><br />
        <label >Email</label><br />
        <input type="email" value={email} onChange={(e) => setEmail(e.target.value)} /><br />
        <label >Message</label><br />
        <textarea className='' value={theme} onChange={(e) => setTheme(e.target.value)} rows={4}></textarea><br />
        <button type='submit'>Envoyer</button>
      </form>
    </div>
    <Footer/>
    </div>
  )
}

export default Travaillez_chez_Bin_edk;
