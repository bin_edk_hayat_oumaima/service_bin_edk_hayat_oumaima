import React from 'react';
import './services.css'
import { Link } from 'react-router-dom';
import Header from '../../components/common/heading/Header';
import Footer from '../../components/footer/Footer';
function Services() {
  return (
     <div className='servicesTopPage'>
      <Header/>
    <div className='servicesTop'>
       
      <h1>Tous les services</h1>
      <div className='servicesDesplay'>
        <img className='imageS'  src="Img4.png" alt="" />
        <img className='iconsArrow'  src="icons8-arrow-64.png" alt="" />
        <div className='servicesPartie'>
        <Link to={'/depannage'}><h4>Dépannage et réparation</h4></Link>
        <p>Un problème de plomberie, d'électricité, de serrurerie… ? Une fuite d’eau, un chauffe-eau à remplacer... Pour toutes les prestations de dépannage ...</p>
        </div>
      </div>
      <div className='servicesDesplay'>
        <img className='imageS'  src="Img2.png" alt="" />
        <img className='iconsArrow'  src="icons8-arrow-64.png" alt="" />
      <div className='servicesPartie'>
        <Link to={'/petits_travaux'}><h4>Petits travaux</h4></Link>
        <p>Des envies de bricolage ou des petits travaux d’embellissement de vos espaces intérieurs et extérieurs (peinture, pose de carrelage, nouvelle installation électrique, parquet ou stores vénitiens ...)</p>
      </div>
      </div>
      <div className='servicesDesplay'>
        <img className='imageS' src="Img1.png" alt="" />
        <img className='iconsArrow' src="icons8-arrow-64.png" alt="" />
      <div className='servicesPartie'>
        <Link to={'/nettoyage'}><h4>Nettoyage</h4></Link>
        <p>Nettoyage de vitres, nettoyage de bureaux, nettoyage de restaurants, nettoyage d’hôtels à Riyad, nettoyage de bâtiments, de logements (nettoyage d’appartements, de maisons, de villas, etc...)</p>
      </div>
      </div>
      <div className='servicesDesplay'>
        <img  className='imageS' src="Img3.png" alt="" />
        <img className='iconsArrow' src="icons8-arrow-64.png" alt="" />
      <div className='servicesPartie'>
        <Link to={'/securite'}><h4>Securite</h4></Link>
        <p>Problème de sécurité ? Chez bin_edk, nous avons la solution ! Notre service de sécurité est fourni. Demandez votre intervention en ligne ou par téléphone : La sécurité privée est attribuée dans les 20 minutes et intervient dans les deux heures.</p>
      </div>
      </div>
    </div>
    <Footer/>
    </div>
  );
}

export default Services;
