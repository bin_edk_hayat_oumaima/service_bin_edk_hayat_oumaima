import React, { useState } from 'react';
import { Email, Lock, Person } from '@mui/icons-material';
import './inscription.css';
import { useDispatch } from 'react-redux';
import {useNavigate } from 'react-router-dom';
import { login } from '../../redux/UserSlice';
import axios from 'axios';

function Inscription() {
  const [prenom, setPrenom] = useState("");
  const [nom, setNom] = useState("");
  const [email, setEmail] = useState("");
  const [email1, setEmail1] = useState(""); // Changement de nom de la fonction setState
  const [password1, setPassword1] = useState(""); // Changement de nom de la fonction setState
  const [password, setPassword] = useState("");
  const [showInscription, setShowInscription] = useState(true);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleToggle = () => {
    setShowInscription(!showInscription);
  };

  const IsValidate = () => {
    let isProceed = true;
    let errorMessage = 'Please enter the value into';
    if (!/^[a-zA-Z]+$/.test(prenom)) {
      isProceed = false;
      errorMessage += " Prénom (letters only)";
    }
    if (!/^[a-zA-Z]+$/.test(nom)) {
      isProceed = false;
      errorMessage += " Nom (letters only)";
    }
    if (!email.trim() || !/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {
      isProceed = false;
      errorMessage += " Email ";
    }
    if (!password.trim() || !/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*]).{8,}$/.test(password)) {
      isProceed = false;
      errorMessage += " Password ";
    }
    if (!isProceed) {
      alert(errorMessage);
    } else {
      alert("Registered successfully.");
    }
    return isProceed;
  };

  const sendEmail = async (e) => {
    e.preventDefault();

    try {
      const formData = {
        prenom,
        nom,
        email,
        password
      };
      if (IsValidate()) {
        const response = await axios.post('http://localhost:5000/users', formData);
        console.log('User added:', response.data);
        navigate('/home');
      } else {
        console.log("Form validation failed. User not added");
      }

      setPrenom('');
      setNom('');
      setEmail('');
      setPassword('');
    } catch (error) {
      console.error("Error adding user", error);
      alert("An error occurred during registration. Please try again later."); // Ajout de la notification d'erreur
    }
  };

  const sendEmailLogin = async (e) => {
    e.preventDefault();
    dispatch(login({
      email: email1,
      password: password1,
      loggedIn: true,
    }));
    if (!email1.trim() || !password1.trim()) { // Utilisation de email1 et password1
      return;
    }
    try {
      const response = await axios.get(`http://localhost:5000/users?email=${email1}&password=${password1}`); // Utilisation de email1 et password1
      if (response.data.length > 0) {
        alert("Your login has been made successfully");
        navigate('/home');
      } else {
        alert('Invalid email or password'); // Correction du message d'erreur
      }
    } catch (err) {
      console.error('Error logging in:', err); // Correction du message d'erreur
      alert("An error occurred during login. Please try again later."); // Ajout de la notification d'erreur
    }
  };

  return (
    <div className='containerInscriptionPage'>
      <div className='containerInscription'>
        {showInscription ? (
          <div className="inscription">
            <div className="inscriptionLeft">
              <h1>Bon retour !</h1>
              <p>Connectez-vous avec votre compte <br />pour nous rejoindre !</p>
              <button className="toggle1Inscription" onClick={handleToggle}>Se connecter</button>
            </div>
            <div className="inscriptionRight">
              <h1>Créer un compte </h1>
              <div className="inscriptionSocial_media">
                <a href="https://www.facebook.com/profile.php?id=61555831454443&mibextid=rS40aB7S9Ucbxw6v"><i className='fab fa-facebook-f'></i></a>
                <a href="https://www.instagram.com/binedk2024?utm_source=qr&igsh=dHg1Mzg4MHNzc3A0"><i className='fab fa-instagram'></i></a>
                <a href="https://twitter.com/bin_edk"><i className='fab fa-twitter'></i></a>
              </div>
              <form className='RegisterFormInscription' onSubmit={sendEmail}>
                <div className='RegisterInputInscription'>
                  <input type='text' placeholder='First Name' value={prenom} onChange={(e) => setPrenom(e.target.value)} required />
                  <Person className='iconsRegisterInscription' />
                </div>
                <div className='RegisterInputInscription'>
                  <input type='text' placeholder='Last Name' value={nom} onChange={(e) => setNom(e.target.value)} required />
                  <Person className='iconsRegisterInscription' />
                </div>
                <div className='RegisterInputInscription'>
                  <input type='email' placeholder='Email' value={email} onChange={(e) => setEmail(e.target.value)} required />
                  <Email className='iconsRegisterInscription' />
                </div>
                <div className='RegisterInputInscription'>
                  <input type='password' placeholder='Password' value={password} onChange={(e) => setPassword(e.target.value)} required />
                  <Lock className='iconsRegisterInscription' />
                </div>
                <button type='submit'>S'inscrire</button>
                <button className="toggle4" onClick={handleToggle}>Se connecter</button>
              </form>
            </div>
          </div>
        ) : (
          <div className="connect">
            <div className="inscriptionLeft">
              <h1>Hello, Friend !</h1>
              <p>Si vous n'avez pas de compte <br />  lancez-vous </p>
              <button onClick={handleToggle}>S'inscrire</button>
            </div>
            <div className="inscriptionRight">
              <h1>Créer un compte </h1>
              <div className="inscriptionSocial_media">
                <a href="https://www.facebook.com/profile.php?id=61555831454443&mibextid=rS40aB7S9Ucbxw6v"><i className='fab fa-facebook-f'></i></a>
                <a href="https://www.instagram.com/binedk2024?utm_source=qr&igsh=dHg1Mzg4MHNzc3A0"><i className='fab fa-instagram'></i></a>
                <a href="https://twitter.com/bin_edk"><i className='fab fa-twitter'></i></a>
              </div>
              <form className='RegisterFormInscription' onSubmit={sendEmailLogin}>
                <div className='RegisterInputInscription'>
                  <input type='email' placeholder='Email' value={email1} onChange={(e) => setEmail1(e.target.value)} required />
                  <Email className='iconsRegisterInscription' />
                </div>
                <div className='RegisterInputInscription'>
                  <input type='password' placeholder='Password' value={password1} onChange={(e) => setPassword1(e.target.value)} required />
                  <Lock className='iconsRegisterInscription' />
                </div>
                <button type='submit' >S'inscrire</button>
              </form>
              <button className="toggle4" onClick={handleToggle}>Se connecter</button>

            </div>
          </div>
        )}
      </div>
      <div className='DepassInscription'>
        <a href="/home">Depass</a>
        <img className='iconsArrow' src="icons8-arrow-64.png" alt="" />
      </div>

    </div>
  );
}

export default Inscription;
