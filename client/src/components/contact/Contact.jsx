import React, { useState } from 'react';
import './contact.css';
import { useNavigate } from 'react-router-dom';

function Contact() {
  const [nom, setNom] = useState("");
  const [tele, setTele] = useState("");
  const [email, setEmail] = useState("");
  const [theme, setTheme] = useState("");
  const [star, setStar] = useState("");
  const [isEditing, setIsEditing] = useState(true);
  const navigate = useNavigate();

  const IsValidate = () => {
    let isproceed = true;
    let errormessage = 'Please enter the value into';
    if (nom === null || nom === '') {
      isproceed = false;
      errormessage += " Nom & Prenom ";
    }
    if (tele === null || tele === '') {  
      isproceed = false;
      errormessage += " Thelephone ";
    }
    if (email === null || email === '') {
      isproceed = false;
      errormessage += " Email ";
    }
    if (!isproceed) {
      alert(errormessage);
    } else {
      if (/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)) {

      } else {
        isproceed = false;
        alert('Please enter the valid email');
      }
    }
    return isproceed;
  };

  const sendEmail = (e) => {
    e.preventDefault();
    const formInfo = {
      nom,
      tele,
      email,
      theme,
      star, 
    };
    if (IsValidate()) {
      fetch("http://localhost:3000/contact", {
        method: "POST",
        headers: { 'content-type': 'application/json' },
        body: JSON.stringify(formInfo)
      })
        .then((res) => {
          alert('successfully.');
          navigate('/home');
        })
        .catch((err) => {
          alert('Failed: ' + err.message);
        });
    }
  };

  return (
    <div className='contactForm' id='contact'>
      <form onSubmit={sendEmail}>
        <h1 className='titleContact'>Se Connecter Avec Nous</h1>
        <div className="input_Contact">
          <i className='fas fa-user icons'></i>
          <input placeholder='Nom & Prenom' value={nom} onChange={(e) => setNom(e.target.value)} type="text" name='user_name' required />
        </div>

        <div className="input_Contact">
          <i className='fas fa-phone-alt icons'></i> 
          <input placeholder='Telephone' value={tele} onChange={(e) => setTele(e.target.value)} type="text" name='user_tele' required /> {/* تغيير نوع الحقل وتحديد القيمة لهاتف */}
        </div>

        <div className="input_Contact">
          <i className='fas fa-envelope icons'></i>
          <input placeholder=' Email' value={email} onChange={(e) => setEmail(e.target.value)} type="text" name='user_email' required />
        </div>
        <div className="input_Contact">
          <i className='fas fa-comments icons'></i>
          <textarea placeholder='Votre Message' value={theme} onChange={(e) => setTheme(e.target.value)} id='message' name='message' rows={4} required />
        </div>
        <div className='experienceTop'>
      <div className="containerEX">
        <div className={isEditing ? "star-widget" : "star-widget hidden"}>
          <input type="radio" name="rate" id="rate-5" checked={star === "5"} onClick={() => setStar("5")} />
          <label htmlFor="rate-5" className="fas fa-star"></label>
          <input type="radio" name="rate" id="rate-4" checked={star === "4"} onClick={() => setStar("4")} />
          <label htmlFor="rate-4" className="fas fa-star"></label>
          <input type="radio" name="rate" id="rate-3" checked={star === "3"} onClick={() => setStar("3")} />
          <label htmlFor="rate-3" className="fas fa-star"></label>
          <input type="radio" name="rate" id="rate-2" checked={star === "2"} onClick={() => setStar("2")} />
          <label htmlFor="rate-2" className="fas fa-star"></label>
          <input type="radio" name="rate" id="rate-1" checked={star === "1"} onClick={() => setStar("1")} />
          <label htmlFor="rate-1" className="fas fa-star"></label>
          <form onSubmit={(e)=>e.preventDefault()}>
          <header></header>
          </form>
        </div>
        </div>
        </div>
        <button className='buttonSubmit' type='submit'><i className='fas fa-paper-plane icons-button'></i>Submit</button>
      </form>
    </div>
  );
}

export default Contact;
