import React from 'react'
import './nettoyage.css'
import { Link } from 'react-router-dom'
import Header from '../../common/heading/Header'
import Footer from '../../footer/Footer'
function Nettoyage() {
  return (
    <div className='nettoyageTop_Components'>
      <Header/>
    <div className='nettoyageTop'>
        <h3><em>Nettoyage</em></h3>
        <div className='nettoyageSecond'>
            <Link style={{marginBottom:'10px'}} className='linkTopNettoyage' to={'/'}>Nettoyage de bureaux</Link> 
            <Link style={{marginBottom:'10px'}} className='linkTopNettoyage' to={'/'}>Nettoyage de locaux</Link>
            <Link style={{marginBottom:'10px'}} className='linkTopNettoyage' to={'/'} >Nettoyage de fin chantier</Link>
            <Link style={{marginBottom:'10px'}} className='linkTopNettoyage' to={'/'}>Nettoyage maison</Link>
        </div>
        <div className='nettoyageSecond'>
            <Link className='linkTopNettoyage' to={'/home'}>J’ai besoin de plusieurs prestations</Link> 
         </div>
    </div>
    <Footer/>
    </div>
  )
}

export default Nettoyage