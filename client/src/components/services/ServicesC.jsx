import React from 'react';
import './servicesC.css';
import Img1 from '../../../src/image/Img1.png'
import Img2 from '../../../src/image/Img2.png'
import Img3 from '../../../src/image/Img3.png'
import Img4 from '../../../src/image/Img4.png'
import { Link } from 'react-router-dom';
const ServicesC = () => {
    return (
        <div className="servicesTopComponent">
            <h1>Tous les services</h1>
            <div className="ServicesComponentCom">
                <div className="serviceCom">
                    <img src={Img4} alt="" />
                    <h3>DÉPANNAGE & RÉPARATION</h3>

                    <p>Un problème de plomberie, d'électricité, de serrurerie…? Chez bin_edk, nous avons la solution!Notre service de dépannage est assuré.Demandez votre dépannage en ligne ou par téléphone .</p>

                    <Link to={'/depannage'}><button className='ButtonService'>Décrivez votre besoin</button></Link>
                </div>
                <div className="serviceCom">
                    <img src={Img2} alt="" />
                    <h3>PETITS TRAVAUX</h3>

                    <p>Monter un meuble ? Accrocher votre miroir sur un mur carrelé, poser du carrelage ou repeindre un mur?Vous pouvez faire appel à nos Pros des petits travaux qui seront là pour vous proposer </p>

                    <Link to={'/petits_travaux'}><button className='ButtonService'>Décrivez votre besoin</button></Link>
                </div>
                <div className="serviceCom">
                    <img src={Img1} alt="" />
                    <h3>NETTOYAGE</h3>

                    <p>Nettoyage de vitres,nettoyage de bureaux,nettoyage de restaurants,nettoyage d'hôtels à Riyad,nettoyage de bâtiments,de logements(nettoyage d’appartements..)</p>

                    <Link to={'/nettoyage'}><button className='ButtonService'>Décrivez votre besoin</button></Link>
                </div>
                <div className="serviceCom">
                    <img src={Img3} alt="" />
                    <h3>SECURITE</h3>

                    <p>Problème de sécurité?Chez bin_edk,nous avons la solution!Notre service de sécurité est fourni. Demandez votre intervention en ligne ou par téléphone La sécurité privée est attribuée dans les 20 minutes .</p>

                    <Link to={'/securite'}><button className='ButtonService'>Décrivez votre besoin</button></Link>

                </div>
            </div>
        </div>




















        // <div className="services" >
        //     <div className="awesome">
        //         <span>Tous les services</span>
        //         <p id='A'><span id='B'>Bin</span><span id='C'>_</span>Edk</p>
        //     </div>
        //     <div className="cards">
        //         <div style={{left:'0rem'}} id='cardT'>
        //         <Card
        //             Img={Img4}
        //             heading={'DÉPANNAGE & RÉPARATION'}
        //             detail={" Un problème de plomberie, d'électricité, de serrurerie… ? Chez bin_edk, nous avons la solution ! Notre service de dépannage est assuré.Demandez votre dépannage en ligne ou par téléphone : un artisan est missionné en 20 minutes et intervient dans les deux heures."} />
        //          <button className='c-button'><Link className='hrefLink' to={'/depannage'} >Décrivez votre besoin</Link></button>
        //         </div>
        //         <div style={{right:'-21rem', bottom:'19rem'}} id='cardB'>
        //         <Card
        //             Img={Img2}
        //             heading={'PETITS TRAVAUX'}
        //             detail={"Monter un meuble ? Accrocher votre miroir sur un mur carrelé, poser du carrelage ou repeindre un mur ? Vous pouvez faire appel à nos Pros des petits travaux qui seront là pour vous proposer leur savoir-faire et avec toujours la garantie d'un travail bien fait !"} />
        //         <button className='c-button'><Link className='hrefLink' to={'/petits_travaux'} >Décrivez votre besoin</Link></button>
        //         </div>
        //         <div style={{right:'-42rem', bottom:'37rem'}} id='cardT'>
        //         <Card
        //             Img={Img1}
        //             heading={'NETTOYAGE'}
        //             detail={"Nettoyage de vitres, nettoyage de bureaux, nettoyage de restaurants, nettoyage d’hôtels à Riyad, nettoyage de bâtiments, de logements (nettoyage d’appartements, de maisons, de villas, etc...)"} />
        //         <button className='c-button'><Link className='hrefLink' to={'/nettoyage'} >Décrivez votre besoin</Link></button>
        //         </div>
        //         <div style={{right:'-63rem', bottom:'54rem'}} id='cardB'>
        //         <Card
        //             Img={Img3}
        //             heading={'SECURITE'}
        //             detail={"Problème de sécurité ? Chez bin_edk, nous avons la solution ! Notre service de sécurité est fourni. Demandez votre intervention en ligne ou par téléphone : La sécurité privée est attribuée dans les 20 minutes et intervient dans les deux heures."} />
        //         <button className='c-button'><Link className='hrefLink' to={'/securite'} >Décrivez votre besoin</Link></button>
        //         </div>
        //     </div>
        // </div>

    )
}

export default ServicesC;