import React from 'react'
import './securite.css'
import { Link } from 'react-router-dom'
import Footer from '../../footer/Footer'
import Header from '../../common/heading/Header'
function Securite() {
  return (
    <div className='securiteTop_Components' >
    <Header/>
    <div className='securiteTop'>
         <h3><em>Securité</em></h3>
        <div className='securiteSecond'>
            <Link style={{marginBottom:'10px'}} className='linkTopSecurite' to={'/garde-villa'}>garde villa</Link> 
            <Link style={{marginBottom:'10px'}} className='linkTopSecurite' to={'/Gardien-d’immeuble'}>Gardien d’immeuble</Link>
            <Link style={{marginBottom:'10px'}} className='linkTopSecurite' to={'/Garde-carrée'} >Garde carrée</Link>
            <Link style={{marginBottom:'10px'}} className='linkTopSecurite' to={'/Garde-du-parc'}>Garde du parc</Link>
        </div>
        <div className='securiteSecond'>
            <Link className='linkTopSecurite' to={'/home'}>J’ai besoin de plusieurs prestations</Link> 
         </div>
         </div>
         <Footer/>
    </div>
  )
}

export default Securite