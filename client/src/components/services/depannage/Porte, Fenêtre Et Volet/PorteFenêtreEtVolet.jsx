import React from 'react'
import Header from '../../../common/heading/Header'
import Footer from '../../../footer/Footer'
import './PorteFenêtreEtVolet.css'
function PorteFenêtreEtVolet() {
  return (
    <div className='PorteFenêtreEtVoletTop_Components'>
      <Header />
      <div className='PorteFenêtreEtVoletTop'>
        <h1><em>Climatisation</em></h1>
        <h3>Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae, ducimus.</h3>
        <div className='PorteFenêtreEtVoletSecond'>
          <Link className='PorteFenêtreEtVoletlinkTop' to={'/PorteFenêtreEtVoletLink'}>Plomberie</Link>
          <Link className='PorteFenêtreEtVoletlinkTop' to={'/Extracteurdairdesalledebain'}>Extracteur d'air de salle de bain</Link>
          <Link className='PorteFenêtreEtVoletlinkTop' to={'/PorteFenêtreEtVoletLink'}>Plomberie</Link>
          <Link className='PorteFenêtreEtVoletlinkTop' to={'/PorteFenêtreEtVoletLink'}>Plomberie</Link>
        </div>
      </div>
      <Footer />
    </div>
  )
}

export default PorteFenêtreEtVolet