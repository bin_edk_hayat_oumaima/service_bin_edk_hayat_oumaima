import React from 'react'
function Head() {
  return (
    <div>
        <section className='head'>
            <div className="container flexSB">
                 <div className="logo">
                    <h1><em>Bin_<span >Edk</span></em></h1>
                 </div>
                 <div className="social">
                    <a href='https://www.facebook.com/profile.php?id=61555831454443&mibextid=rS40aB7S9Ucbxw6v'><i  className='fab fa-facebook-f icon'></i></a>
                    <a href='https://www.instagram.com/binedk2024?utm_source=qr&igsh=dHg1Mzg4MHNzc3A0'><i className='fab fa-instagram icon'></i></a>
                    <a href={`https://web.whatsapp.com/send?phone=${774463661}`}><i className='fab fa-whatsapp icon'></i></a>
                    <a href="https://twitter.com/bin_edk"><i className='fab fa-twitter icon'></i></a>
                 </div>
            </div>
        </section>
    </div>
  )
}

export default Head ;